module.exports = (req, res, next) => {
    if (req.session.account) {
        res.locals.account = req.session.account;
    }
    if (req.session.toast) {
        res.locals.toast = req.session.toast;
        req.session.toast = null;
    }
    next();
};
