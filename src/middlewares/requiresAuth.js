module.exports = (...allowed) => {
    const isAllowed = role => allowed.length === 0 || allowed.indexOf(role) > -1;
    return (req, res, next) => {
        if (req.session && req.session.account) {
            if (isAllowed(req.session.account.role)) {
                return next();
            }
            return res.render('static/errors/403');
        }
        if (req.originalUrl !== '/') {
            req.session.redirectUrl = req.originalUrl;
        }
        return res.redirect('/login');
    }
};
