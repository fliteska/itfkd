module.exports = (err, req, res, next) => {
    if (err && err.code === "EBADCSRFTOKEN") {
        req.session.toast = {
            content: 'Something went wrong',
            className: 'error',
        };
        return res.redirect(req.originalUrl);
    } else if (err) {
      next(err);
    } else {
      next(req, res);
    }
};
