const bcrypt = require('bcrypt');

module.exports = (password, callback) => {
  bcrypt.hash(password, 10, callback);
};
