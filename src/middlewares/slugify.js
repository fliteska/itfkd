const slugify = require('slugify');

module.exports = str => slugify(str, {
  replacement: '-',
  remove: /[*+~.()'"!:@]/g,
  lower: true,
});
