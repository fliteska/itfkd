const { validationResult } = require('express-validator/check');
module.exports = (redirectUrl) => {
    return (req, res, next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            const errorArr = errors.array();
            const formErrors = [];
            errorArr.forEach(({ msg }) => {
                formErrors.push(msg);
            });
            req.session.toast = {
                content: formErrors.join('<br />'),
                status: 'Error',
                className: 'danger',
            };
            return res.redirect(redirectUrl);
        }
        next();
    };
};
