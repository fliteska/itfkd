const AwsSesMail = require('aws-ses-mail');

const sesMail = new AwsSesMail();

sesMail.setConfig({
    accessKeyId: process.env.SES_ACCESS_KEY,
    secretAccessKey: process.env.SES_SECRET_KEY,
    region: 'eu-west-1',
});

module.exports = (options, callback) => sesMail.sendEmailByHtml(options, callback);
