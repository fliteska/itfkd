const express = require('express');

const router = express.Router();

const hashPassword = require('../../middlewares/hashPassword');
const requiresAuth = require('../../middlewares/requiresAuth');

const service = require('../../db/account/service');

router.get('/', requiresAuth(), (req, res) => {
    res.render('static/access/account/edit');
});

router.post('/password', requiresAuth(), (req, res) => {
    const { _id } = req.session.account;
    const {
        currentPassword,
        newPassword,
        newPasswordConfirm,
    } = req.body;

    if (newPassword !== newPasswordConfirm) {
        req.session.toast = {
            content: 'Password not updated',
            className: 'error',
        };
        res.redirect('/account');
    }
    return service.getOne(_id)
        .then(({ email }) => service.authenticate(email, currentPassword, (err, account) => {
            if (err || !account) {
                req.session.toast = {
                    content: 'Invalid credentials',
                    className: 'error',
                };
                return res.redirect('/login');
            }
            return hashPassword(newPassword, (err2, hash) => {
                if (!err2) {
                    return service.update(_id, {
                        password: hash,
                    }).then(() => {
                        req.session.toast = {
                            content: 'Password updated',
                            className: 'success',
                        };
                        res.redirect('/account');
                    });
                }
                console.log(err2);
                return res.status(400);
            });
        }));
});

module.exports = router;
