const express = require('express');

const router = express.Router();

const accountService = require('../../db/account/service');

router.get('/', (req, res) => {
    res.render('static/access/reset');
});

router.post('/', (req, res) => {
    const {
        email,
        username,
    } = req.body;
    accountService.index({
        email,
        username,
    }, 1, 1)
        .then((data) => {
            if (data.docs.length === 0) {
                req.session.toast = {
                    content: 'Unable to reset password',
                    className: 'error',
                };
                return res.redirect('/password-reset');
            }
            const account = data.docs[0];
            const randomisedPassword = Math.random().toString(36).slice(-8);
            return hashPassword(randomisedPassword, (err, hash) => {
                accountService.update(account._id, { /* eslint-disable-line */
                    password: hash,
                }).then(() => {
                    const emailData = {
                        from: 'ITFKD Team <team@itfkd.net>',
                        to: account.email,
                        subject: 'ITFKD Password has been reset',
                        content: `Your password has been reset, here is your temporary password: ${randomisedPassword}`,
                    };

                    sendEmail(emailData, (sesError, sesData) => {
                        console.log(sesError, sesData);
                        req.session.toast = {
                            content: 'An email has been sent to the registered email address',
                            className: 'secondary',
                        };
                        return res.redirect('/password-reset');
                    });
                });
            });
        });
});

module.exports = router;
