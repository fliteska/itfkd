const express = require('express');

const router = express.Router();

const accountService = require('../../db/account/service');

router.get('/', (req, res) => {
    res.render('static/access/login');
});

router.post('/', (req, res) => {
    const {
        email,
        password,
    } = req.body;
    return accountService.authenticate(email, password, (err, account) => {
        if (err || !account) {
            req.session.toast = {
                content: 'Invalid credentials',
                className: 'error',
            };
            res.redirect('/login');
        } else {
            req.session.account = account;
            if (req.session.redirectUrl) {
                res.redirect(req.session.redirectUrl);
            } else {
                res.redirect('/admin');
            }
        }
    });
});

module.exports = router;
