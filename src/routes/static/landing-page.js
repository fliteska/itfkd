const express = require('express');
const { check } = require('express-validator/check');
const axios = require('axios');

const handleFormErrors = require('../../middlewares/handleFormErrors');
const sendEmail = require('../../utils/sendEmail');

const router = express.Router();

router.get('/', (req, res) => {
    res.render('static/landing-page', {
        title: 'Web Developer',
        skills: ['js-square', 'node', 'react', 'vuejs', 'sass', 'python'],
        projects: [
            {
                title: 'Hub',
                description: 'A content API built with ExpressJS.',
                technologies: ['js-square'],
                img: '/images/hub-api.png',
            },
            {
                title: 'CSS Framework',
                description: 'A work-in-progress CSS framework, because we always need more CSS frameworks...',
                technologies: ['js-square', 'sass'],
                img: '/images/css-framework.png',
            },
            {
                title: 'Enthroned RPG',
                description: 'A text based adventure game built with React and PHP.',
                technologies: ['react', 'php', 'sass'],
                img: '/images/enthroned-rpg.png',
            },
        ],
    });
});

router.post('/', [
    check('email', 'Email address is not valid.').isEmail(),
    check('message', 'Message cannot be empty.').not().isEmpty(),
], handleFormErrors('/#contact'), (req, res) => {
    const {
        subject,
        name,
        email,
        phone,
        recaptcha,
        message,
    } = req.body;

    const gUrl = 'https://www.google.com/recaptcha/api/siteverify';
    const secret = process.env.RECAPTCHA_SECRET_KEY;
    const recaptchaUrl = `${gUrl}?secret=${secret}&response=${recaptcha}`;

    axios.post(recaptchaUrl, {}, {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
        },
    }).then((response) => {
        if (response.data.success) {
            const emailData = {
                from: 'ITFKD Team <team@itfkd.net>',
                to:'itsverynaic@gmail.com',
                subject: `ITFKD Message: ${subject}`,
                template: `${__dirname}/contact.html`,
                templateArgs: {
                    subject,
                    name,
                    email,
                    phone,
                    message,
                },
            };

            return sendEmail(emailData, (sesError, sesData) => {
                req.session.toast = {
                    content: 'Message sent!',
                    className: 'success',
                };
                return res.redirect('/');
            });
        }
        req.session.toast = {
            content: 'Message not sent!',
            className: 'danger',
        };
        return res.redirect('/');
    });
});

module.exports = router;
