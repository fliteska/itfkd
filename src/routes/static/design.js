const express = require('express');

const router = express.Router();

router.use((req, res, next) => {
    res.locals.menuItems = [
        { href: '/design/buttons', label: 'Buttons' },
        { href: '/design/navbars', label: 'Navbars' },
    ];
    next();
});

router.get('/buttons', (req, res) => {
    res.render('static/design/buttons', {
        layout: 'design',
    });
});

router.get('/navbars', (req, res) => {
    res.render('static/design/navbars', {
        layout: 'design',
    });
});

module.exports = router;
