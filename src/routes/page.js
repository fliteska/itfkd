const articleService = require('../db/articles/service');

module.exports = (req, res) => {
    const { slug } = req.params;
    articleService.getBySlug(slug)
        .then((article) => {
            if (!article) {
                res.render('static/errors/404');
            } else {
                res.render(`templates/${article.template}`, {
                    article,
                });
            }
        });
};
