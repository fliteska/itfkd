const express = require('express');

const router = express.Router();

const service = require('../../db/articles/service');

router.get('/', (req, res) => {
    const page = parseInt(req.query.page) || 1;
    const limit = parseInt(req.query.limit) || 10;
    service.index({}, page, limit)
        .then((data) => {
            res.render('admin/articles/list', {
                data,
            });
        });
});

router.get('/create', (req, res) => {
    res.render('admin/articles/edit');
});

router.post('/create', (req, res) => {
    const { accountId } = req.session;
    const {
        title,
        template,
        content,
    } = req.body;
    service.create({
        author: accountId,
        title,
        template,
        content,
    }).then(() => {
        req.session.toast = {
            content: 'Article Created',
            className: 'secondary',
        };
        res.redirect('/admin/articles');
    });
});

router.get('/:id', (req, res) => {
    const { id } = req.params;
    service.getOne(id)
        .then((data) => {
            res.render('admin/articles/edit', {
                data,
            });
        });
});

router.post('/:id', (req, res) => {
    const { id } = req.params;
    const {
        title,
        template,
        content,
    } = req.body;
    service.update(id, {
        title,
        template,
        content,
    }).then(() => {
        req.session.toast = {
            content: 'Article updated',
            className: 'secondary',
        };
        res.redirect('/admin/articles');
    });
});

module.exports = router;
