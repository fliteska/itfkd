const express = require('express');

const router = express.Router();
router.get('/', (req, res) => {
    res.render('admin/dashboard');
});
router.use('/articles', require('./article'));

module.exports = router;
