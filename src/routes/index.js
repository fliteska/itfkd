/* eslint-disable */
const express = require('express');
const basicAuth = require('express-basic-auth');
const csrf = require('csurf');
require('express-group-routes');
const router = express.Router();

const SUPERUSER_NAME = process.env.SUPERUSER_NAME || 'admin';
const SUPERUSER_PASSWORD = process.env.SUPERUSER_PASSWORD || 'password';

const apiAuth = basicAuth({
    users: {
        [SUPERUSER_NAME]: SUPERUSER_PASSWORD,
    },
});
const requiresAuth = require('../middlewares/requiresAuth');
router.use(require('../middlewares/populateFromSession'));

// router.group('/api/v1', (router) => {
//     router.use('/accounts', apiAuth, require('../db/account/routes/api'));
//     router.use('/articles', apiAuth, require('../db/articles/routes/api'));
// });

router.use(csrf({
    cookie: {
        httpOnly: true,
        secure: true,
    },
}));

router.use((req, res, next) => {
    res.locals.csrftoken = req.csrfToken();
    next();
});

router.group((router) => {
    router.use('/', require('./static/landing-page'));
    router.use('/login', require('./access/login'));
    router.use('/account', require('./access/account'));
    router.use('/password-reset', require('./access/password-reset'));
    router.use('/design', require('./static/design'));
    router.use('/admin', requiresAuth('admin'), require('./admin'));
    router.get('/:slug', requiresAuth(), require('./page'));
});

router.use(require('../middlewares/pageNotFound'));

module.exports = router;
