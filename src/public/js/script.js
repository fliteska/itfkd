/* eslint-disable */
const navbars = document.querySelectorAll('.navbar');
navbars.forEach((navbar) => {
    const toggle = navbar.querySelector('.toggle');
    toggle.addEventListener('click', () => {
        const target = toggle.getAttribute('data-target');
        try {
            navbar.querySelector(`${target}`).classList.toggle('active');
        } catch (e) {
            console.error(`Navbar Invalid Target: ${target} not found.`);
        }
    });
});

const toasts = document.querySelectorAll('.toast');
let toastTimeout;
toasts.forEach((toast) => {
    toastTimeout = setTimeout(() => {
        toast.remove();
    }, 3000);
    toast.querySelector('.fa-times').addEventListener('click', () => {
        toast.remove();
        clearTimeout(toastTimeout);
    });
});

const contactForm = document.querySelector('.contact-form');

if (contactForm) {
    contactForm.addEventListener('submit', (e) => {
        const recaptchaResponse = grecaptcha.getResponse();
        contactForm.querySelector('.recaptcha').value = recaptchaResponse;
    });
}
