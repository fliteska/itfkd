const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const timestamps = require('mongoose-timestamp');

const schema = new mongoose.Schema({
    title: String,
    slug: String,
    author: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Account',
    },
    template: String,
    content: String,
});

schema.plugin(mongoosePaginate);
schema.plugin(timestamps);

const model = mongoose.model('Article', schema);

module.exports = model;
