const Moment = require('moment');
const Model = require('./model');

const slugify = require('../../middlewares/slugify');

exports.create = ({
    author,
    title,
    content,
    template,
}) => new Model({
    author,
    title,
    slug: slugify(title),
    content,
    template,
    timestamp: new Moment(),
}).save();

exports.index = (query, page, limit) => Model.paginate(query, {
    page: parseInt(page, 10),
    limit: parseInt(limit, 10),
    populate: ['author'],
});

exports.getBySlug = slug => Model.findOne({
    slug,
}).populate(['author']);

exports.getOne = _id => Model.findById(_id).populate(['author']);

exports.update = (_id, data) => Model.findByIdAndUpdate(_id, data);

exports.delete = _id => Model.findByIdAndDelete(_id);

exports.destroy = () => Model.deleteMany();
