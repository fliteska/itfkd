const Model = require('./model');

exports.create = ({
    username,
    email,
    password,
}) => new Model({
    username,
    email,
    password,
}).save();


exports.index = (query, page, limit) => Model.paginate(query, {
    page: parseInt(page, 10),
    limit: parseInt(limit, 10),
});

exports.getOne = _id => Model.findById(_id);

exports.update = (_id, data) => Model.findByIdAndUpdate(_id, data);

exports.delete = _id => Model.findByIdAndDelete(_id);

exports.destroy = () => Model.deleteMany();

exports.model = Model;

exports.authenticate = (id, password, callback) => Model.authenticate(id, password, callback);
