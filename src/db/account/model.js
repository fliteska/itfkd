const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const timestamps = require('mongoose-timestamp');
const bcrypt = require('bcrypt');

const schema = new mongoose.Schema({
    username: {
        type: String,
        unique: true,
        required: true,
        trim: true,
        lowercase: true,
    },
    email: {
        type: String,
        unique: true,
        lowercase: true,
        required: true,
        trim: true,
    },
    password: {
        type: String,
        required: true,
        select: false,
    },
    role: String,
});

schema.plugin(mongoosePaginate);
schema.plugin(timestamps);

// hashing a password before saving it to the database
/* eslint-disable */
schema.pre('save', function (next) {
    bcrypt.hash(this.password, 10, (err, hash) => {
        if (err) {
            return next(err);
        }
        this.password = hash;
        return next();
    });
});
/* eslint-enable */

const model = mongoose.model('Account', schema);

model.authenticate = (email, password, callback) => {
    model.findOne({ email })
        .select(['email', 'password', 'role'])
        .exec((err, account) => {
            if (err) {
                return callback(err);
            } if (!account) {
                const err2 = new Error('Account not found.');
                err2.status = 401;
                return callback(err2);
            }
            const accountPassword = account.password;
            delete account.password;
            return bcrypt.compare(password, accountPassword, (err3, result) => {
                if (result === true) {
                    return callback(null, account);
                }
                return callback(err3);
            });
        });
};

module.exports = model;
