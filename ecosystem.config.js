module.exports = {
    apps: [{
        name: 'ITFKD',
        script: 'index.js',
        instances: 1,
        autorestart: true,
        watch: false,
        max_memory_restart: '1G',
        env: {
            NODE_ENV: 'development',
            PORT: 3001,
            SITE_TITLE: 'Ian Derbyshire',
        },
        env_production: {
            NODE_ENV: 'production',
            PORT: 3000,
            SITE_TITLE: 'Ian Derbyshire',
        },
    }],
};
